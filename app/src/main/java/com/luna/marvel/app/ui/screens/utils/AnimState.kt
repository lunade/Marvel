package com.luna.marvel.app.ui.screens.utils

enum class AnimState {
    IDLE, FINISH, START
}